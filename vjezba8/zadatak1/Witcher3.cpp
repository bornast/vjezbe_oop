#include "pch.h"
#include "Witcher3.h"

using namespace OSS;

Witcher3::Witcher3()
{
	type = "Rpg and OpenWorld";
}


Witcher3::~Witcher3()
{
}

vector<string> Witcher3::platforms()
{
	return _platforms;
}

void Witcher3::addPlatform(string platform)
{
	_platforms.push_back(platform);
}

string Witcher3::getType() {
	return type;
}