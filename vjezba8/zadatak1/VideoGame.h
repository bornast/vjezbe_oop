#pragma once
#include <string>
#include <vector>

using namespace std;

namespace OSS {

	class VideoGame {
	public:
		virtual string getType() = 0;
		virtual vector<string> platforms() = 0;
		virtual ~VideoGame() = 0;
		virtual void addPlatform(string str) = 0;
	};

	VideoGame::~VideoGame() {};
}