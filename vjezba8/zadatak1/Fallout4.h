#pragma once
#include "Rpg.h"

namespace OSS {
	class Fallout4 :
		public Rpg
	{
	private:
		vector<string> _platforms;
	public:
		Fallout4();
		~Fallout4();
		vector<string> platforms();
		void addPlatform(string str);
	};
}
