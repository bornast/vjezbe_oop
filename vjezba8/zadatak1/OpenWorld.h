#pragma once
#include "VideoGame.h"

namespace OSS {
	class OpenWorld :
		virtual public VideoGame
	{
	protected:
		string type = "Open World";
	public:
		OpenWorld();
		~OpenWorld();
		string getType();
		virtual void addPlatform(string str) {};
		//virtual vector<string> platforms() { };
	};
}
