#pragma once
#include "Rpg.h"
#include "OpenWorld.h"

namespace OSS {
	class Witcher3 :
		public Rpg, public OpenWorld
	{
	private:
		vector<string> _platforms;
		string type;
	public:
		Witcher3();
		~Witcher3();
		vector<string> platforms();
		void addPlatform(string str);
		string getType();
	};
}