#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

using namespace std;

template <typename T>
class Zbroj
{
public:
	Zbroj() { cout << "obicna" << endl; };
	T zbroji(T a, T b)
	{
		return a + b;
	}
};

template <>
class Zbroj<char>
{
public:
	Zbroj() { cout << "specijalizirana" << endl; };
	char zbroji(char a, char b)
	{
		if (isdigit(a) && isdigit(b)) {
			return ((a - '0') + (b - '0') + '0');
		}
		return (a - '0') + (b - '0');
	}
};
