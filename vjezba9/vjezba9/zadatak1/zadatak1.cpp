#include "pch.h"
#include <iostream>
#include "Stack.h"

int main()
{
	Stack<int> stack;
	
	stack.push(5);
	stack.pop();
	stack.pop();
	stack.push(55);
	cout << "stack size: " << stack.getSize() << endl;	
	cout << "top element: " << stack.getTopElement() << endl;
	
}

