#pragma once
#include <iostream>
#include <vector>

using namespace std;

template <typename T>

class Stack
{
private:
	int _defaultSize;
	int _maxSize;
	int _size;
	int _top;
	T *_stackPtr;
	vector<T> _stack;
	bool stackEmpty()
	{
		return _size <= 0;
	}
	bool stackFull()
	{
		return _size >= _maxSize;
	}
public:
	Stack()
	{
		_defaultSize = 10;
		_maxSize = 1000;
		_stack.reserve(_defaultSize);
	}
	void push(T element)
	{
		if (!stackFull())
		{
			_stack.push_back(element);
			_stackPtr = &_stack.back();
			_size++;
		}
		else cout << "stack is full" << endl;
		
	}
	void pop()
	{
		if (!stackEmpty())
		{
			_stack.pop_back();
			_size--;
			if (!stackEmpty()) _stackPtr = &_stack.back();
			else _stackPtr = NULL;
		}
		else cout << "stack is empty" << endl;
	}

	int getSize()
	{
		return _size;
	}
	T getTopElement()
	{
		if (!stackEmpty())
			return _stack.back();
		else
			cout << "stack is empty" << endl;
	}
	
};

