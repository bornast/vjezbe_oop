#pragma once
#include <string>
#include <iostream>

using namespace std;

namespace oop {

	class Vec3
	{

	private:
		float _x;
		float _y;
		float _z;
		static int _counter;

	public:
		Vec3(float x, float y, float z);
				
		Vec3 operator+(const Vec3& other);
		Vec3 operator-(const Vec3& other);
		Vec3 operator*(int a);
		Vec3 operator/(int a);
		Vec3& operator+=(const Vec3& other);
		Vec3& operator-=(const Vec3& other);
		Vec3& operator*=(const Vec3& other);
		Vec3& operator/=(const Vec3& other);
		bool operator==(const Vec3& other);
		bool operator!=(const Vec3& other);
		float operator*(const Vec3& other);
		float& operator[](int index);
		void normalizacija();
		static int getCounter();

		float getX();
		float getY();
		float getZ();

		friend ostream & operator << (ostream & os, Vec3 & p) {
			os << "(" << p._x << ", " << p._y << ", " << p._z << ") ";
			return os;
		}

		friend istream & operator >> (istream & is, Vec3 & p) {
			cout << "upisi x" << endl;
			is >> p._x;
			cout << "upisi y" << endl;
			is >> p._y;
			cout << "upisi z" << endl;
			is >> p._z;
			return is;
		}

	};

}


