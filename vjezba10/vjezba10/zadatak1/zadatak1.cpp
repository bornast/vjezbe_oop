#include "pch.h"
#include <iostream>
#include <exception>
#include <ostream>
#include <string>
#include <algorithm>
#include <fstream>

using namespace std;

class djeljenjeNula : public exception
{
	virtual const char* what() const throw()
	{
		return "djeljenje s nula";
	}
};

class unosInta : public exception
{
	virtual const char* what() const throw()
	{
		return "trebate unijeti int";
	}
};

class podrzaniOperand : public exception
{
	virtual const char* what() const throw()
	{
		return "nije podrzani operand";
	}
};

class logger {

public:
	void log(string txt) {
		ofstream fout("errors.log", ios_base::out | ios_base::app);
		fout << txt << endl;
		cout << txt << endl;
		fout.close();
	}
};

class miscs {

public:
	int dohvatiInt() {

		int number;

		cout << "unesite int" << endl;

		if (!(cin >> number))
			throw unosInta();

		return number;
	}
	char dohvatiOperand() {

		char operand;

		cout << "unesite nesto od (*,/,+,-)" << endl;
		cin >> operand;
		
		if (operand != '*' && operand != '+' && operand != '-' && operand != '/')
			throw podrzaniOperand();

		return operand;
	}
	void ispisiRezultat(int a, int b, char operand)
	{
		if (operand == '*') cout << a * b << endl;
		else if (operand == '+') cout << a + b << endl;
		else if (operand == '-') cout << a - b << endl;
		else if (operand == '/') {
			if (b == 0)
				throw djeljenjeNula();
			cout << a / b << endl;
		}
			
	}

};


int main()
{
	logger logger;
	miscs test;
	while (1 == 1)
	{
		try {
			int number1 = test.dohvatiInt();
			int number2 = test.dohvatiInt();
			char operand = test.dohvatiOperand();
			test.ispisiRezultat(number1, number2, operand);			
		}
		catch (exception& ex)
		{
			logger.log(ex.what());
			return 0;
		}
	}

	
	return 0;

}

