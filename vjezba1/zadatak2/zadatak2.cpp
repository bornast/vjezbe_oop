#include "pch.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <math.h> 

using namespace std;


int extractIntValueFromInput(string message) {

	int returnValue;

	cout << message << endl;
	cin >> returnValue;
	while (!cin.good())
	{
		cin.clear();
		cin.ignore(INT_MAX, '\n');
		cout << message << endl;
		cin >> returnValue;

	}

	return returnValue;

}

struct Student {
	string Id;
	string ime;
	string spol;
	int ocjeneKvizova[2];
	int ocjenaSredinaSemestra;
	int ocjenaKrajSemestra;

	int ukupanBrojBodova()
	{
		return ocjeneKvizova[0] + ocjeneKvizova[1] + ocjenaSredinaSemestra + ocjenaKrajSemestra;

	}

	double prosjekBodova() {
		return (ocjeneKvizova[0] + ocjeneKvizova[1] + ocjenaSredinaSemestra + ocjenaKrajSemestra) / 4;
	}

};

vector<Student> students;

bool checkIfStudentExists(string studentId) {

	for (Student& student : students) {
		if (student.Id == studentId) {
			return true;
		}
	}
	return false;
}

void addStudent() {

	Student student;
	cout << "unesite id studenta " << endl;
	cin >> student.Id;

	while (checkIfStudentExists(student.Id)) {
		cout << "unesite id studenta " << endl;
		cin >> student.Id;
	}
	cout << "unesite ime studenta " << endl;
	cin >> student.ime;
	cout << "unesite spol studenta " << endl;
	cin >> student.spol;

	student.ocjeneKvizova[0] = extractIntValueFromInput("unesite ocjenu kviza prvog semestra studenta");
	student.ocjeneKvizova[1] = extractIntValueFromInput("unesite ocjenu kviza drugog semestra studenta");
	student.ocjenaSredinaSemestra = extractIntValueFromInput("unesite ocjenu kviza drugog semestra studenta");
	student.ocjenaKrajSemestra = extractIntValueFromInput("unesite ocjenu sredine semestra studenta");

	students.push_back(student);
}

void removeStudentById() {

	string studentId;
	cout << "unesite id studenta " << endl;
	cin >> studentId;

	if (students.empty() == true) {
		cout << "svi studenti su izbrisani" << endl;
		return;
	}

	while (true) {

		for (Student& student : students) {
			if (student.Id == studentId) {
				//students.remove(student);
				student.Id = "";
				return;
			}
		}
		cout << "unesite ispravan id studenta " << endl;
		cin >> studentId;
	}

}

void updateStudent() {

	string studentId;
	cout << "unesite id studenta " << endl;
	cin >> studentId;

	if (students.empty() == true) {
		cout << "svi studenti su izbrisani" << endl;
		return;
	}

	for (Student& student : students) {
		if (student.Id == studentId) {
			student.ocjeneKvizova[0] = extractIntValueFromInput("unesite ocjenu kviza prvog semestra studenta");
			student.ocjeneKvizova[1] = extractIntValueFromInput("unesite ocjenu kviza drugog semestra studenta");
			student.ocjenaSredinaSemestra = extractIntValueFromInput("unesite ocjenu kviza drugog semestra studenta");
			student.ocjenaKrajSemestra = extractIntValueFromInput("unesite ocjenu sredine semestra studenta");
		}
	}


}

void studentAverageGrade() {

	string studentId;
	cout << "unesite id studenta " << endl;
	cin >> studentId;

	if (students.empty() == true) {
		cout << "svi studenti su izbrisani" << endl;
		return;
	}

	for (Student& student : students) {
		if (student.Id == studentId) {
			cout << "prosjek bodova: " << student.prosjekBodova();
		}
	}


}

void displayStudent(Student student) {
	cout << "Student " << student.ime << " (id:" << student.Id << ") " << endl;
	cout << "Ocjena prvog kviza: " << student.ocjeneKvizova[0] << endl;
	cout << "Ocjena drugog kviza: " << student.ocjeneKvizova[1] << endl;
	cout << "Ocjena sredina semestra: " << student.ocjenaSredinaSemestra << endl;
	cout << "Ocjena kraj semestra: " << student.ocjenaKrajSemestra << endl;
	cout << "Ukupan broj bodovoa: " << student.ukupanBrojBodova() << endl;
}

void displayAllStudents() {

	for (Student student : students) {
		displayStudent(student);
	}

}

void displayStudentWithHighestPoints() {

	Student studentWithHighestPt = students[0];

	for (Student student : students) {
		if (student.prosjekBodova() > studentWithHighestPt.prosjekBodova()) {
			studentWithHighestPt = student;
		}
	}

	displayStudent(studentWithHighestPt);

}

void displayStudentWithLowestPoints() {
	Student studentWithLowestPt = students[0];

	for (Student student : students) {
		if (student.prosjekBodova() < studentWithLowestPt.prosjekBodova()) {
			studentWithLowestPt = student;
		}
	}

	displayStudent(studentWithLowestPt);
}

void getyStudentById() {

	string studentId;
	cout << "unesite id studenta " << endl;
	cin >> studentId;


	while (true) {
		for (Student student : students) {
			if (student.Id == studentId) {
				displayStudent(student);
				return;
			}
		}
		cout << "unesite id studenta " << endl;
		cin >> studentId;
	}

}

void displaySortedStudentsByPoints() {

	Student swap;

	for (int i = 0; i < students.size() - 1; i++) {
		for (int j = 0; j < students.size(); j++) {
			if (students[i].ukupanBrojBodova() > students[j].ukupanBrojBodova()) {
				swap = students[i];
				students[i] = students[j];
				students[j] = swap;
			}
		}
	}

	displayAllStudents();

}

void displayMenu() {

	cout << "1.Dodaj novi zapis" << endl;
	cout << "2.Ukloni zapis" << endl;
	cout << "3.Azuriraj zapis" << endl;
	cout << "4.Prikazi sve zapise" << endl;
	cout << "5.Prosjek bodova za studenta" << endl;
	cout << "6.Prikazi studenta s najvecim brojem bodova" << endl;
	cout << "7.Prikazi studenta s najmanjim brojem bodova" << endl;
	cout << "8.Pronadi studenta po ID-u" << endl;
	cout << "9.Sortiraj zapise po broju bodova(total)" << endl;
	cout << "10.Izlaz" << endl;

}

int main()
{
	int option;

	while (true) {
		displayMenu();
		cin >> option;

		if (option < 1 || option > 10) break;
		else if (option == 1) addStudent();
		else if (option == 2) removeStudentById();
		else if (option == 3) updateStudent();
		else if (option == 4) displayAllStudents();
		else if (option == 5) studentAverageGrade();
		else if (option == 6) displayStudentWithHighestPoints();
		else if (option == 7) displayStudentWithLowestPoints();
		else if (option == 8) getyStudentById();
		else if (option == 9) displaySortedStudentsByPoints();
		else if (option == 10) break;
	}
}

