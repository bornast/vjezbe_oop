#pragma once

#include "pch.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int generateRandomNumber(int maxValue) {

	return (rand() % maxValue) + 1;
}

int extractIntValueFromInput(string message, int min, int max) {

	int returnValue;

	cout << message << endl;
	cin >> returnValue;
	while (!cin.good() || (returnValue < min || returnValue > max))
	{
		cin.clear();
		cin.ignore(INT_MAX, '\n');
		cout << "error, " << message << endl;
		cin >> returnValue;

	}

	return returnValue;

}

vector<int> generateVectorValues(int a = 0, int b = 100, int n = 5, bool generateRandomly = true) {

	//edge cases
	a = a < 0 || a > 100 ? 0 : a;
	b = b < a || b > 100 ? 100 : b;

	vector<int> res;

	if (generateRandomly) {
		for (int i = 0; i < n; i++) {
			res.push_back(generateRandomNumber(b));
		}
	}
	else {
		for (int i = 0; i < n; i++) {
			res.push_back(extractIntValueFromInput("enter value: ", a, b));
		}
	}

	return res;

}

void printVector(vector<int> vctr) {

	vector<int>::iterator it;

	for (it = vctr.begin(); it < vctr.end(); it++) {
		cout << *it << endl;
	}

}

vector<int> generateDifferenceVector(vector<int> vector1, vector<int> vector2) {

	vector<int> res;

	vector<int>::iterator it;

	sort(vector2.begin(), vector2.end());

	for (it = vector1.begin(); it < vector1.end(); it++) {
		if (!binary_search(vector2.begin(), vector2.end(), *it)) {
			res.push_back(*it);
		}
	}

	return res;
}
