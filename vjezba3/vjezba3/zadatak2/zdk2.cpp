#include "pch.h"
#include "Header.h"

using namespace std;

int main()
{
	srand(time(0));

	vector<int> res1 = generateVectorValues();
	vector<int> res2 = generateVectorValues();
	vector<int> differenceVector = generateDifferenceVector(res1, res2);

	printVector(differenceVector);
}

