#include "pch.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <math.h> 

using namespace std;

struct Producent {

	string name;
	string movie;
	int year;
	int count;

};

vector<Producent> producers;

vector<pair<string, int>> producersPopularity;

int extractIntValueFromInput(string message) {

	int returnValue;

	cout << message << endl;
	cin >> returnValue;
	while (!cin.good())
	{
		cin.clear();
		cin.ignore(INT_MAX, '\n');
		cout << "gre�ka, " << message << endl;
		cin >> returnValue;

	}

	return returnValue;

}

void printProducers() {

	for (vector<Producent>::iterator it = producers.begin(); it != producers.end(); ++it)
		cout << ' ' << (*it).name;

}

void sortMostCommonProducers() {

	for (size_t i = 0; i < producersPopularity.size() - 1; i++) {
		for (size_t j = i + 1; j < producersPopularity.size(); j++) {

			if (producersPopularity[i].second < producersPopularity[j].second) {
				producersPopularity[j].swap(producersPopularity[i]);
			}

		}

	}
}

void printMostCommonProducer() {

	sortMostCommonProducers();

	for (size_t i = 0; i < producersPopularity.size() - 1; i++) {

		if (i == 0 || producersPopularity[i].second == producersPopularity[i - 1].second)
			cout << "producent: " << producersPopularity[i].first << " (" << producersPopularity[i].second << ")" << endl;
		else
			break;


	}

}

void addProducersPopularity(string name) {

	for (vector<pair<string, int>>::iterator it = producersPopularity.begin(); it != producersPopularity.end(); ++it) {
		if ((*it).first == name) {
			(*it).second++;
			return;
		}
	}

	pair <string, int> producent;
	producent.first = name;
	producent.second = 1;

	producersPopularity.push_back(producent);

}

void enterProducers() {


	int producersToAdd = extractIntValueFromInput("unesite broj producenata");

	while (producersToAdd > 0) {

		Producent producent;

		cout << "unesite ime producenta" << endl;
		cin >> producent.name;

		cout << "unesite ime filma producenta" << endl;
		cin >> producent.movie;

		producent.year = extractIntValueFromInput("unesite godinu");

		producers.push_back(producent);

		producersToAdd--;

		addProducersPopularity(producent.name);
	}


}

int main()
{


	enterProducers();
	printMostCommonProducer();

}
