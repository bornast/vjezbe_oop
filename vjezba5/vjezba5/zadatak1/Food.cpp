#include "pch.h"
#define _CRT_SECURE_NO_WARNINGS
#include "Food.h"
#include <iostream>
#include <math.h>
#include <ctime>
#include <algorithm>

using namespace std;


Potrosnja::Potrosnja()
{

}

Potrosnja::Potrosnja(int year, int month, float kg)
{
	_year = year;
	_month = month;
	_kg = kg;
}

// ************************ GETTERS ************************
const int Potrosnja::getYear()
{
	return _year;
}
const int Potrosnja::getMonth()
{
	return _month;
}
const float Potrosnja::getKg()
{
	return _kg;
}

// ********************** GETTERS END ***********************

Food::Food() {

}

Food::Food(string vrsta, string naziv, float kolicinaVoda, float protein, 
	float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane)
{
	_vrsta = vrsta;
	_naziv = naziv;
	_kolicinaVoda = kolicinaVoda;
	_protein = protein;
	_mast = mast;
	_ugljikohidrat = ugljikohidrat;
	_rokTrajanja = rokTrajnja;
	_dnevnaKolicinaHrana = dnevnaKolicinaHrane;
	_potrosnjaHrane = new Potrosnja[toAllocate(_rokTrajanja)];
}

Food::~Food() {
	delete[] _potrosnjaHrane;
}

Food::Food(const Food &f2) {
	_vrsta = f2._vrsta;
	_naziv = f2._naziv;
	_kolicinaVoda = f2._kolicinaVoda;
	_protein = f2._protein;
	_mast = f2._mast;
	_ugljikohidrat = f2._ugljikohidrat;
	_rokTrajanja = f2._rokTrajanja;
	_dnevnaKolicinaHrana = f2._dnevnaKolicinaHrana;

	int potrosnjaLength = sizeof(f2._potrosnjaHrane) / sizeof(f2._potrosnjaHrane[0]);
	_potrosnjaHrane = new Potrosnja[potrosnjaLength];
	copy(f2._potrosnjaHrane, f2._potrosnjaHrane + potrosnjaLength, _potrosnjaHrane);	
}

const int Food::toAllocate(string date)
{
	int dateYear = stoi(date.substr(date.length() - 4));
	
	int currentYear = getCurrentYear();

	int yearsToAllocate = dateYear - currentYear == 0 ? 1 : dateYear - currentYear;

	return yearsToAllocate * 2 * 12;

}

void Food::uvecajPotrosnju()
{
	_dnevnaKolicinaHrana++;
}

void Food::smanjiPotrosnju()
{
	if(_dnevnaKolicinaHrana >= 1)
		_dnevnaKolicinaHrana--;
}


void Food::dodajPodatkeOPotrosnji(Potrosnja potrosnja)
{
	if (!currentYear(potrosnja.getYear()))
		return;

	if (consuptionExists(potrosnja.getYear(), potrosnja.getMonth()))
		return;
	
	_potrosnjaHrane[_currentIndex] = potrosnja;

	_currentIndex++;

}


bool Food::currentYear(int year)
{		
	return getCurrentYear() == year;
}

bool Food::consuptionExists(int year, int month)
{
	for (int i = 0; i < _currentIndex; i++)
	{
		if (_potrosnjaHrane[i].getYear() == year && _potrosnjaHrane[i].getMonth() == month)
			return true;
	}

	return false;
}


const float Food::consuptionRaisedDecreased()
{
	float sum = 0;
	int currentYear = getCurrentYear();

	for (int i = 0; i < _currentIndex; i++)
	{
		if (_potrosnjaHrane[i].getYear() == currentYear)
		{
			sum += _potrosnjaHrane[i].getKg();
		}
			
	}

	sum = sum / (_currentIndex * 30);
	float tenPercent = (sum * 0.1);

	float difference = sum - _dnevnaKolicinaHrana;

	
	
	return difference > 0 && difference > tenPercent ? difference : difference < 0 && difference > tenPercent*(-1) ? difference : 0;

}

const int Food::getCurrentYear()
{
	time_t now = time(0);
	tm *ltm = localtime(&now);
	//return 1970 + ltm->tm_year;
	return 1900 + ltm->tm_year;
}

const void Food::printFoodDetails()
{
	cout << "vrsta: " << _vrsta << endl;
	cout << "naziv: " << _naziv << endl;
	cout << "kolicina vode: " << _kolicinaVoda << endl;
	cout << "protein: " << _protein << endl;
	cout << "mast: " << _mast << endl;
	cout << "ugljikohidrat: " << _ugljikohidrat << endl;
	cout << "rok trajanja: " << _rokTrajanja << endl;
	cout << "dnevna potrosnja: " << _dnevnaKolicinaHrana << endl;

	for (int i = 0; i < _currentIndex; i++)
	{
		cout << "potrosnja za godinu: " << _potrosnjaHrane[i].getYear() << " i mjesec " 
			<< _potrosnjaHrane[i].getMonth() << " iznosi " << _potrosnjaHrane[i].getKg() << endl;
	}
		
}


void Food::regulateDailyConsuption()
{
	float consuptionCheck = consuptionRaisedDecreased();

	if (consuptionCheck < 0)
	{
		_dnevnaKolicinaHrana -= consuptionCheck;
	}
	else if (consuptionCheck > 0)
	{
		_dnevnaKolicinaHrana += consuptionCheck;
	}

}