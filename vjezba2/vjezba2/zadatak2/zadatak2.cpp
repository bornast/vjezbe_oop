#include "pch.h"
#include <iostream>
#include <string>

using namespace std;


void printArray(int *arr, int len) {

	for (int i = 0; i < len; i++)
		cout << arr[i] << endl;
}

void sortByEvenNumbers(int *arr, int len) {

	int swap;
	for (int i = 0; i < len - 1; i++) {
		for (int j = i + 1; j < len; j++) {
			if (arr[i] % 2 != 0) {
				swap = arr[i];
				arr[i] = arr[j];
				arr[j] = swap;
			}
		}

	}

}

int main()
{
	int arr2[] = { 4, 1, 2, 4, 2, 6, 4, 5, 5, 91, 123, 5, 7 };
	int arr2Len = sizeof(arr2) / sizeof(arr2[0]);
	sortByEvenNumbers(arr2, arr2Len);
	printArray(arr2, arr2Len);
}
