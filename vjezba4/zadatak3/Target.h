#pragma once
#ifndef TARGET_H
#define TARGET_H

#include "../zdk2/Weapon.h";

class Target
{

private:
	Point _point;
	bool _hit;
	double _height;
public:
	Target(Point point, double height = 85);

	const Point getPoint();
	const double getHeight();
	void hit();
};

#endif