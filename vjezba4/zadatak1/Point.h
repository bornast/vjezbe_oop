#pragma once
#ifndef POINT_H
#define POINT_H

class Point
{

private:
	double _x;
	double _y;
	double _z;

public:
	Point(int minValue, int maxValue);
	Point(double x = 0, double y = 0, double z = 0);

	const double calculateDistanceFrom2D(Point point);	
	const double calculateDistanceFrom3D(Point point);
	
	const double getX();
	const double getY();
	const double getZ();
	
};

#endif

