#include "pch.h"
#include "Point.h"
#include <iostream>
#include <math.h>

using namespace std;


Point::Point(int minValue, int maxValue)
{	
	_x = rand() % (maxValue - minValue + 1) + minValue;
	_y = rand() % (maxValue - minValue + 1) + minValue;
	_z = rand() % (maxValue - minValue + 1) + minValue;
	
}

Point::Point(double x, double y, double z)
{
	_x = x;
	_y = y;
	_z = z;
}

const double Point::calculateDistanceFrom2D(Point point)
{	
	return sqrt(pow((_x - point._x), 2) + pow((_y - point._y), 2));
}
	
const double Point::calculateDistanceFrom3D(Point point)
{
	return sqrt(pow((point._x - _x), 2) + pow((point._y - _y), 2) + pow((point._z - _z), 2));
}

const double Point::getZ() { return _x; }
const double Point::getY(){ return _y; }
const double Point::getX() { return _z; }
	
