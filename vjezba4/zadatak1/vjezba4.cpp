#include "pch.h"
#include <iostream>
#include <iomanip>
#include "Point.h";

using namespace std;

int main()
{
	srand(time(0));

	Point point1 = Point(10.0, 15.2, 14.4);
	Point point2 = Point(-10, 30);
	
	cout << point1.calculateDistanceFrom2D(point2) << endl;
	cout << point1.calculateDistanceFrom3D(point2) << endl;

}

