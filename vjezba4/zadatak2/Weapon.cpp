#include "pch.h"
#include "Weapon.h"
#include <iostream>
#include <math.h>

using namespace std;

Weapon::Weapon(Point point, int numberOfBullets)
{
	_point = point;
	_numberOfBullets = numberOfBullets;
	_bulletsLeft = numberOfBullets;
}

void Weapon::shoot() { 
	if (_bulletsLeft == 0) {
		reload();
	}
}
void Weapon::reload() { 
	_bulletsLeft = _numberOfBullets;
}

const Point Weapon::getPoint() {
	return _point;
}