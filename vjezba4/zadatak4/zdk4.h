#pragma once

class Point
{

private:
	double _x;
	double _y;
	double _z;

public:
	Point(int maxValue);
	Point(double x = 0, double y = 0, double z = 0);

	double calculateDistanceFrom2D(Point point);
	double calculateDistanceFrom3D(Point point);

	double getX();
	double getY();
	double getZ();

};

class Weapon
{

private:
	Point _point;
	int _numberOfBullets;
	int _bulletsLeft;

public:
	void shoot();
	void reload();
	Weapon(Point point, int numberOfBullets = 7);

	Point getPoint();

};


class Target
{

private:
	Point _point;
	bool _hit;
	double _height;
public:
	Target(Point point, double height = 85);

	Point getPoint();
	double getHeight();
	void hit();
};
