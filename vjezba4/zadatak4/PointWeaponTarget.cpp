#include "pch.h"
#include "PointWeaponTarget.h"
#include <iostream>
#include <math.h>

using namespace std;

Point::Point(int maxValue)
{
	_x = (rand() % maxValue) + 1;
	_y = (rand() % maxValue) + 1;
	_z = (rand() % maxValue) + 1;
}

Point::Point(double x, double y, double z)
{
	_x = x;
	_y = y;
	_z = z;
}

double Point::calculateDistanceFrom2D(Point point)
{
	return sqrt(pow((_x - point._x), 2) + pow((_y - point._y), 2));
}

double Point::calculateDistanceFrom3D(Point point)
{
	return sqrt(pow((point._x - _x), 2) + pow((point._y - _y), 2) + pow((point._z - _z), 2));
}

double Point::getZ() { return _x; }
double Point::getY() { return _y; }
double Point::getX() { return _z; }

Weapon::Weapon(Point point, int numberOfBullets)
{
	_point = point;
	_numberOfBullets = numberOfBullets;
	_bulletsLeft = numberOfBullets;
}

void Weapon::shoot() {
	if (_bulletsLeft == 0) {
		reload();
	}
}
void Weapon::reload() {
	_bulletsLeft = _numberOfBullets;
}

Point Weapon::getPoint() {
	return _point;
}


Target::Target(Point point, double height)
{
	_point = point;
	_height = height;
	_hit = false;
}

Point Target::getPoint() {
	return _point;
}

double Target::getHeight() {
	return _height;
}

void Target::hit() {
	_hit = true;
}