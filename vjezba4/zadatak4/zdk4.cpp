#include "pch.h"
#include <iostream>
#include "../zdk3/Target.h"

using namespace std;

int main()
{
	
	int n = 5;

	Point point = Point(-30, 100);
	Weapon weapon = Weapon(point);

	int targetsHit = 0;
	
	for (int i = 0; i < n; i++) {
		
		Target target = Target(Point(-30, 100));
		if (weapon.getPoint().getZ() >= target.getPoint().getZ() && weapon.getPoint().getZ() < (target.getHeight() + target.getPoint().getZ())) {
			target.hit();
			targetsHit++;
		}
	}

	cout << targetsHit << endl;
	


}

