#pragma once
#include "Mesni.h"

namespace oop {

	class Meso :
		public Mesni
	{
	public:
		Meso(float potrosnjaSamostalnogJela, float potrosnjaKaoDioDrugogJela, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Meso();
	};

}