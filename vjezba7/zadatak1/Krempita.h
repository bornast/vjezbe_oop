#pragma once
#include "Kolaci.h"

namespace oop {

	class Krempita :
		public Kolaci
	{
	public:
		Krempita(float potrosnjaKaoDesert, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Krempita();
	};

}