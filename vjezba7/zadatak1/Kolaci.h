#pragma once
#include "Food.h"

namespace oop {

	class Kolaci :
		public Food
	{
	protected:
		float _potrosnjaKaoDesert;
	public:
		Kolaci(float potrosnjaKaoDesert, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Kolaci();
		virtual void print(std::ostream& os) const
		{
			os << "Naziv: " << _naziv << endl;
			os << "Potrosnja kao desert: " << _potrosnjaKaoDesert << " " << "kg" << endl;
		}
		friend istream & operator>>(istream & is, Kolaci& m) {
			is >> m._potrosnjaKaoDesert >> m._vrsta >> m._naziv
				>> m._kolicinaVoda >> m._protein >> m._mast
				>> m._ugljikohidrat >> m._rokTrajanja >> m._dnevnaKolicinaHrana;
			return is;
		}
	};
}

